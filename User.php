<?php
require_once('config.php');

class User
{
	private $id;
	private $name;
	private $password;

	function __construct($args)
	{
		if (isset($args['id']) && filter_var($args['id'], FILTER_VALIDATE_INT))
			$this->id = $args['id'];
		if (isset($args['name']))
			$this->name = $args['name'];
		if (isset($args['password']))
			$this->password = $args['password'];
	}

	/**
	 * Jelszó hasítása és sózása
	 */
	private function makeHash(&$password)
	{
		$hash = '$6$'.base64_encode(mcrypt_create_iv(12, MCRYPT_DEV_URANDOM));
		$password = crypt($this->password, $hash);
	}

	/**
	 * A felhasználó jelszavának ellenőrzése
	 */
	public function isValid()
	{
		try {
			$dbh = new PDO(DB_DSN, DB_USER, DB_PASS);
			$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

			$stmt = $dbh->prepare(
				"SELECT password FROM user WHERE name = :name"
			);
			$stmt->bindValue(':name', $this->name);

			$stmt->execute();
			$result = $stmt->fetch(PDO::FETCH_ASSOC);
			
			$dbh = null;
		} catch (PDOException $e) {
			print 'Hiba: ' . $e->getMessage() . "<br/>";
			die();
		}
		return (
			crypt($this->password, $result['password']) == $result['password']
		);
	}

	/**
	 * A felhasználó adatainak tárolása
	 */
	public function persist()
	{
		if (!isset($this->name, $this->password))
			die('Hiba: kevés adat!');
		try {
			$dbh = new PDO(DB_DSN, DB_USER, DB_PASS);
			$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

			$stmt = $dbh->prepare(
				"INSERT INTO user (name, password) VALUES (:name, :password)"
			);
			$stmt->bindValue(':name', $this->name);
			$stmt->bindParam(':password', $password);

			$this->makeHash($password);
			$stmt->execute();

			$dbh = null;
		} catch (PDOException $e) {
			print 'Hiba: ' . $e->getMessage() . "<br/>";
			die();
		}
	}

	/**
	 * A felhasználó törlése
	 */
	public function delete()
	{
		if (!isset($this->id))
			die('Hiba: kevés adat!');
		try {
			$dbh = new PDO(DB_DSN, DB_USER, DB_PASS);
			$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

			$result = $dbh->exec("DELETE FROM user WHERE id = $this->id");
			
			$dbh = null;
		} catch (PDOException $e) {
			print 'Hiba: ' . $e->getMessage() . "<br/>";
			die();
		}
		return $result;
	}

	public function getName()
	{
		if (!isset($this->id))
			die('Hiba: kevés adat!');
		try {
			$dbh = new PDO(DB_DSN, DB_USER, DB_PASS);
			$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

			$user = $dbh->query("SELECT name FROM user WHERE id = $this->id")
				->fetch();
			
			$dbh = null;
		} catch (PDOException $e) {
			print 'Hiba: ' . $e->getMessage() . "<br/>";
			die();
		}
		if (!$user)
			die('Hiba!');
		return $this->name = $user['name'];
	}

	public function update()
	{
		try {
			$dbh = new PDO(DB_DSN, DB_USER, DB_PASS);
			$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

			$stmt = $dbh->prepare("UPDATE user SET name = :name, "
			                     ."password = :password WHERE id = $this->id");
			$stmt->bindValue(':name', $this->name);
			$stmt->bindParam(':password', $password);

			$this->makeHash($password);
	
			$stmt->execute();

			$dbh = null;
		} catch (PDOException $e) {
			print 'Hiba: ' . $e->getMessage() . "<br/>";
			die();
		}
	}
}
