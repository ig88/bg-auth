<?php
require('../config.php');
require('../User.php');

if (empty($_POST['name']) || empty($_POST['password']))
	die('Azonosítás szükséges!'); // Egyik érték sem lehet "0"!

$user = new User(array(
	'name' => $_POST['name'],
	'password' => $_POST['password']
));

if (!$user->isValid())
	die('Hibás adatok!');

?>

<!DOCTYPE html>
<head>
	<meta charset="utf-8">
	<title></title>
	<link rel="stylesheet" href="style.css">
</head>
<body>
	<p>Sikeres belépés. Ezt az oldalt csak regisztrált felhasználók láthatják.</p>
</body>
