<?php
require('../config.php');
require('../User.php');

$uid = filter_var($_GET['id'], FILTER_VALIDATE_INT);
if (!$uid)
	die('Hiba!');

$user = new User(array('id' => $uid));
$uname = $user->getName();

?>
<!DOCTYPE html>
<head>
	<meta charset="utf-8">
	<title>Adminisztráció</title>
	<link rel="stylesheet" href="style.css">
</head>
<body>
	<h1>Felhasználó szerkesztése</h1>
	<form method="post"
	      enctype="application/x-www-form-urlencoded"
	      action="update.php">
		<input type="hidden" name="id" value="<?=$uid?>">
		<p><label>Név: <input name="name" value="<?=$uname?>"></label></p>
		<p><label>Új jelszó: <input name="password"></label></p>
		<p><button>Rögzítés</button></p>
	</form>
	<p><a href="admin.php">Vissza az admin oldalra</a></p>
</body>
