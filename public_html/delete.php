<?php
require('../config.php');
require('../User.php');

$user = new User(array('id' => $_GET['id']));

if ($user->delete())
	$msg = 'Felhasználó törölve.';
else
	$msg = 'Sikertelen.';

?>
<!DOCTYPE html>
<head>
	<meta charset="utf-8">
	<title>Adminisztráció</title>
	<link rel="stylesheet" href="style.css">
</head>
<body>
	<p><?=$msg?></p>
	<p><a href="admin.php">Vissza az admin oldalra</a></p>
</body>
