<?php
require('../config.php');

try {
	$dbh = new PDO(DB_DSN, DB_USER, DB_PASS);
	$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	$users = $dbh->query('SELECT id, name FROM user')->fetchAll();
	
	$dbh = null;
} catch (PDOException $e) {
	print 'Hiba: ' . $e->getMessage() . '<br/>';
	die();
}


?>
<!DOCTYPE html>
<head>
	<meta charset="utf-8">
	<title>Adminisztráció</title>
	<link rel="stylesheet" href="style.css">
</head>
<body>
	<h1>Regisztrált felhasználók</h1>
	<p>Szerkesztéshez kattintson a névre!</p>
	<ul>
		<?php foreach ($users as $user): ?>
			<li><a href="edit.php?id=<?=$user['id']?>"><?=$user['name']?></a> (<a href="delete.php?id=<?=$user['id']?>">törlés</a>)</li>
		<?php endforeach; ?>
	</ul>
	<p><a href="reg.html">Új felhasználó hozzáadása</a></p>
</body>
