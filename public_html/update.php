<?php
require('../config.php');
require('../User.php');

if (!$_POST['id'] || !$_POST['name'] || !$_POST['password'])
	die('Hiányzó adat!');

$user = new User(array(
	'id' => $_POST['id'],
	'name' => $_POST['name'],
	'password' => $_POST['password']
	));
$user->update();
?>
<!DOCTYPE html>
<head>
	<meta charset="utf-8">
	<title>Adminisztráció</title>
	<link rel="stylesheet" href="style.css">
</head>
<body>
	<p>Sikeres mentés.</p>
	<p><a href="admin.php">Vissza az admin oldalra</a></p>
</body>
