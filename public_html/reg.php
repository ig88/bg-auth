<?php
require('../config.php');
require('../User.php');

$user = new User(array(
	'name' => $_POST['name'],
	'password' => $_POST['password']
));

$user->persist();
?>
<!DOCTYPE html>
<head>
	<meta charset="utf-8">
	<title>Regisztráció</title>
	<link rel="stylesheet" href="style.css">
</head>
<body>
	<p>Sikeres regisztráció.</p>
	<p><a href="/">Vissza a bejelentkezésre</a></p>
</body>
